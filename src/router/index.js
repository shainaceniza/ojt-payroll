import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Calendar from '@/components/Calendar'
import Employees from '@/components/Employees'
import Reports from '@/components/Reports'
import Recruitment from '@/components/Recruitment'
import Payroll from '@/components/Payroll'
import Help from '@/components/Help'
import Nitish from '@/components/profiles/Nitish'
import Guy from '@/components/profiles/Guy'
import Anne from '@/components/profiles/Anne'
import Tine from '@/components/profiles/Tine'
import Kent from '@/components/profiles/Kent'
import Mc from '@/components/profiles/Mc'
import Joy from '@/components/profiles/Joy'
import Mikaela from '@/components/profiles/Mikaela'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/calendar',
      name: 'Calendar',
      component: Calendar
    },
    {
      path: '/employees',
      name: 'Employees',
      component: Employees
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/reports',
      name: 'Reports',
      component: Reports
    },
    {
      path: '/recruitment',
      name: 'Recruitment',
      component: Recruitment
    },
    {
      path: '/payroll',
      name: 'Payroll',
      component: Payroll
    },
    {
      path: '/help',
      name: 'Help',
      component: Help
    },
    {
      path: '/profiles/nitish',
      name: 'Nitish',
      component: Nitish
    },
    {
      path: '/profiles/guy',
      name: 'Guy',
      component: Guy
    },
    {
      path: '/profiles/anne',
      name: 'Anne',
      component: Anne
    },
    {
      path: '/profiles/Tine',
      name: 'Tine',
      component: Tine
    },
    {
      path: '/profiles/Kent',
      name: 'Kent',
      component: Kent
    },
    {
      path: '/profiles/Mc',
      name: 'Mc',
      component: Mc
    },
    {
      path: '/profiles/Joy',
      name: 'Joy',
      component: Joy
    },
    {
      path: '/profiles/Mikaela',
      name: 'Mikaela',
      component: Mikaela
    }
  ]
})
