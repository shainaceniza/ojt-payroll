import Slide from './Slide'
import Payroll from './Payroll'
import PieChart from './Charts/Piechart'
import BarChart from './Charts/Barchart'

export default {
	/*eslint-disable*/
  Slide,
  Payroll,
  PieChart,
  BarChart
}